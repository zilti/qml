;; -*- geiser-scheme: chicken -*-
(import (r7rs))

(define-library (qml lowlevel)
  (import (scheme base))
  (import srfi-1)
  (import (chicken foreign))
  (import foreigners)
  (import list-comprehensions)
  (export DosQEventLoopProcessEventFlagProcessAllEvents
		  DosQEventLoopProcessEventFlagExcludeUserInputEvents
		  DosQEventLoopProcessEventFlagProcessExcludeSocketNotifiers
		  DosQEventLoopProcessEventFlagProcessAllEventsWaitForMoreEvents
		  DosQtConnectionTypeAutoConnection
		  DosQtConnectionTypeDirectConnection
		  DosQtConnectionTypeQueuedConnection
		  DosQtConnectionTypeBlockingConnection
		  DosQtConnectionTypeUniqueConnection

      dos_slot_macro
      dos_signal_macro

      c_array_convert

      dos_parameterdefinition_create
      dos_parameterdefinition_name
      dos_signaldefinition_create
      dos_signaldefinitions_create
      dos_signaldefinitions_delete
      dos_slotdefinition_create
      dos_slotdefinitions_create
      dos_slotdefinitions_delete
      dos_propertydefinition_create
      dos_propertydefinitions_create
      dos_propertydefinitions_delete
		  dos_qcoreapplication_application_dir_path
		  dos_qcoreapplication_process_events
		  dos_qcoreapplication_process_events_timed
		  dos_qguiapplication_create
		  dos_qguiapplication_exec
		  dos_qguiapplication_quit
		  dos_qguiapplication_delete
		  dos_qapplication_create
		  dos_qapplication_exec
		  dos_qapplication_quit
		  dos_qapplication_delete
		  dos_qquickstyle_set_style
		  dos_qquickstyle_set_fallback_style
		  dos_qobject_signal_emit
      dos_qobject_objectName
      dos_qobject_setObjectName
		  dos_qobject_delete
		  dos_qobject_deleteLater
      dos_qobject_property
      dos_qobject_setProperty
      qml_qobject_findChild
      dos_qobject_connect_lambda_static
      dos_qobject_connect_lambda_with_context_static
      dos_qobject_connect_static
      dos_qobject_disconnect_static
      dos_qobject_disconnect_with_connection_static
		  dos_qvariant_create
		  dos_qvariant_create_int
		  dos_qvariant_create_bool
		  dos_qvariant_create_string
		  dos_qvariant_create_qobject
		  dos_qvariant_create_float
		  dos_qvariant_setInt
		  dos_qvariant_setBool
		  dos_qvariant_setFloat
		  dos_qvariant_setString
		  dos_qvariant_setQObject
		  dos_qvariant_toInt
		  dos_qvariant_toBool
		  dos_qvariant_toString
		  dos_qvariant_toFloat
		  dos_qvariant_toQObject
		  dos_qvariant_isnull
		  dos_qvariant_assign
		  dos_qvariant_create_qvariant
		  dos_qvariant_delete
		  dos_qqmlcontext_baseUrl
		  dos_qqmlcontext_setcontextproperty
		  dos_qurl_create
		  dos_qurl_to_string
		  dos_qurl_isValid
		  dos_qurl_delete
		  dos_qpixmap_create
		  dos_qpixmap_create_width_and_height
		  dos_qpixmap_create_qpixmap
		  dos_qpixmap_load
		  dos_qpixmap_loadFromData
		  dos_qpixmap_fill
		  dos_qpixmap_assign
		  dos_qpixmap_isNull
		  dos_qpixmap_delete
		  dos_qquickimageprovider_create
		  dos_qquickimageprovider_delete
		  dos_qqmlapplicationengine_create
		  dos_qqmlapplicationengine_load
		  dos_qqmlapplicationengine_load_url
		  dos_qqmlapplicationengine_load_data
		  dos_qqmlapplicationengine_add_import_path
		  dos_qqmlapplicationengine_context
      qml_qqmlapplicationengine_root
		  dos_qqmlapplicationengine_addImageProvider
		  dos_qqmlapplicationengine_delete
		  dos_qquickview_create
		  dos_qquickview_show
		  dos_qquickview_source
		  dos_qquickview_set_source_url
		  dos_qquickview_set_source
		  dos_qquickview_set_resize_mode
		  dos_qquickview_rootContext
		  dos_qquickview_delete
		  dos_qmetaobject_create
		  dos_qmetaobject_invoke_method
		  dos_qmetaobject_delete
      dos_qmetaobject_connection_delete
		  dos_qobject_qmetaobject
		  dos_qabstractitemmodel_qmetaobject
		  dos_qabstracttablemodel_qmetaobject
		  dos_qabstractlistmodel_qmetaobject
		  dos_qmodelindex_create
		  dos_qmodelindex_create_qmodelindex
		  dos_qmodelindex_row
		  dos_qmodelindex_column
		  dos_qmodelindex_isValid
		  dos_qmodelindex_data
		  dos_qmodelindex_parent
		  dos_qmodelindex_child
		  dos_qmodelindex_sibling
		  dos_qmodelindex_assign
		  dos_qmodelindex_delete
		  dos_qabstractitemmodel_create
		  dos_qabstractitemmodel_setData
		  dos_qabstractitemmodel_flags
		  dos_qabstractitemmodel_headerData
		  dos_qabstractlistmodel_create
		  dos_qabstractlistmodel_index
		  dos_qabstractlistmodel_parent
		  dos_qabstractlistmodel_columnCount
		  dos_qabstracttablemodel_create
		  dos_qabstracttablemodel_index
		  dos_qabstracttablemodel_parent)
  (begin
    (foreign-declare "#include <DOtherSide/DOtherSideTypes.h>")
    (foreign-declare "#include <DOtherSide/DOtherSide.h>")
    (foreign-declare "#include <QtWidgets/QApplication>")
    (foreign-declare "#include <QtCore/Qt>")
    (foreign-declare "#include <QtCore/QString>")
    (foreign-declare "#include <QtCore/QObject>")
    (foreign-declare "#include <QtQml/QQmlApplicationEngine>")
    (define-foreign-type DosQVariant "DosQVariant")
    (define-foreign-type DosQModelIndex "DosQModelIndex")
    (define-foreign-type DosQAbstractListModel "DosQAbstractListModel")
    (define-foreign-type DosQAbstractItemModel "DosQAbstractItemModel")
    (define-foreign-type DosQAbstractTableModel "DosQAbstractTableModel")
    (define-foreign-type DosQQmlApplicationEngine "DosQQmlApplicationEngine")
    (define-foreign-type DosQQuickView "DosQQuickView")
    (define-foreign-type DosQQmlContext "DosQQmlContext")
    (define-foreign-type DosQHashIntQByteArray "DosQHashIntQByteArray")
    (define-foreign-type DosQUrl "DosQUrl")
    (define-foreign-type DosQMetaObject "DosQMetaObject")
    (define-foreign-type DosQMetaObjectConnection "DosQMetaObjectConnection")
    (define-foreign-type DosQMetaObjectInvokeMethodCallback (function void (c-pointer)))
    (define-foreign-type DosQObject "DosQObject")
    (define-foreign-type DosQObjectConnectLambdaCallback (function void
                                                                   (c-pointer
                                                                    int
                                                                    (c-pointer (c-pointer DosQVariant)))))
    (define-foreign-type DosQQuickImageProvider "DosQQuickImageProvider")
    (define-foreign-type DosPixmap "DosPixmap")
    (define-foreign-type DosQPointer "DosQPointer")

    (define-foreign-type RequestPixmapCallback (function void
                                                         (c-string
                                                          (c-pointer int) ; width
                                                          (c-pointer int) ; height
                                                          int ; requestedWidth
                                                          int ; requestedHeight
                                                          (c-pointer DosPixmap))))
    (define-foreign-type DObjectCallback (function void
                                                   ((c-pointer DosQObject)
                                                    (c-pointer DosQVariant)
                                                    int
                                                    pointer-vector)))
    (define-foreign-type RowCountCallback (function void
                                                    ((c-pointer DosQAbstractItemModel)
                                                     (c-pointer DosQModelIndex)
                                                     (c-pointer int))))
    (define-foreign-type ColumnCountCallback (function void
                                                       ((c-pointer DosQAbstractItemModel)
                                                        (c-pointer DosQModelIndex)
                                                        (c-pointer int))))
    (define-foreign-type DataCallback (function void
                                                ((c-pointer DosQAbstractItemModel)
                                                 (c-pointer DosQModelIndex)
                                                 int
                                                 (c-pointer DosQVariant))))
    (define-foreign-type SetDataCallback (function void
                                                   ((c-pointer DosQAbstractItemModel) ;; Is that correct?
                                                    (c-pointer DosQModelIndex)
                                                    (c-pointer DosQVariant)
                                                    int
                                                    (c-pointer bool))))
    (define-foreign-type RoleNamesCallback (function void
                                                     ((c-pointer DosQAbstractItemModel)
                                                      (c-pointer DosQHashIntQByteArray))))
    (define-foreign-type FlagsCallback (function void
                                                 ((c-pointer DosQAbstractItemModel)
                                                  (c-pointer DosQModelIndex)
                                                  (c-pointer int))))
    (define-foreign-type HeaderDataCallback (function void
                                                      ((c-pointer DosQAbstractItemModel)
                                                       int ; section
                                                       int ; orientation
                                                       int ; role
                                                       (c-pointer DosQVariant))))
    (define-foreign-type IndexCallback (function void
                                                 ((c-pointer DosQAbstractItemModel)
                                                  int ; row
                                                  int ; column
                                                  (c-pointer DosQModelIndex) ; parent
                                                  (c-pointer DosQModelIndex)))) ; result
    (define-foreign-type ParentCallback (function void
                                                  ((c-pointer QAbstractItemModel)
                                                   (c-pointer DosQModelIndex)
                                                   (c-pointer DosQModelIndex))))
    (define-foreign-type HasChildrenCallback (function void
                                                       ((c-pointer QAbstractItemModel)
                                                        (c-pointer DosQModelIndex)
                                                        (c-pointer bool))))
    (define-foreign-type CanFetchMoreCallback (function void
                                                        ((c-pointer QAbstractItemModel)
                                                         (c-pointer DosQModelIndex)
                                                         (c-pointer bool))))
    (define-foreign-type FetchMoreCallback (function void
                                                     ((c-pointer QAbstractItemModel)
                                                      (c-pointer DosQModelIndex))))

    (define-foreign-type CreateDObject (function void
                                                 (int
                                                  (c-pointer DosQObject)
                                                  pointer-vector
                                                  pointer-vector)))
    (define-foreign-type DeleteDObject (function void
                                                 (int
                                                  (c-pointer DosQObject))))

    (define-foreign-type DosQVariantArray (struct "DosQVariantArray"))
    (define-foreign-type QmlRegisterType (struct "QmlRegisterType"))
    (define-foreign-type ParameterDefinition (struct "ParameterDefinition"))
    (define-foreign-type SignalDefinition (struct "SignalDefinition"))
    (define-foreign-type SignalDefinitions (struct "SignalDefinitions"))
    (define-foreign-type SlotDefinition (struct "SlotDefinition"))
    (define-foreign-type SlotDefinitions (struct "SlotDefinitions"))
    (define-foreign-type PropertyDefinition (struct "PropertyDefinition"))
    (define-foreign-type PropertyDefinitions (struct "PropertyDefinitions"))
    (define-foreign-type DosQAbstractItemModelCallbacks (struct "DosQAbstractItemModelCallbacks"))

    (define-foreign-type DosQEventLoopProcessEventFlag (enum "DosQEventLoopProcessEventFlag"))
    (define DosQEventLoopProcessEventFlagProcessAllEvents #x00)
    (define DosQEventLoopProcessEventFlagExcludeUserInputEvents #x01)
    (define DosQEventLoopProcessEventFlagProcessExcludeSocketNotifiers #x02)
    (define DosQEventLoopProcessEventFlagProcessAllEventsWaitForMoreEvents #x03)

    (define-foreign-type DosQtConnectionType (enum "DosQtConnectionType"))
    (define DosQtConnectionTypeAutoConnection 0)
    (define DosQtConnectionTypeDirectConnection 1)
    (define DosQtConnectionTypeQueuedConnection 2)
    (define DosQtConnectionTypeBlockingConnection 3)
    (define DosQtConnectionTypeUniqueConnection #x80)

    (define dos_slot_macro
      (foreign-lambda c-string "dos_slot_macro"
        c-string))
    (define dos_signal_macro
      (foreign-lambda c-string "dos_signal_macro"
        c-string))

    ;; Helpers
    (define c_array_get
      (foreign-lambda* (c-pointer DosQVariant) (((c-pointer (c-pointer DosQVariant)) array)
                                  (int index))
        "C_return(array[index]);"))

    (define (c_array_convert c-array num-items)
      (reverse
       (fold
        (lambda (index array)
          (cons (c_array_get c-array index) array))
        '() (range 0 num-items))))

    ;; Functions
    ;; ParameterDefinition
    (define dos_parameterdefinition_create
      (foreign-primitive scheme-object ((c-string name)
                                        (int metaType))
        "char* pdname = (char*) calloc(1, strlen(name)+1);"
        "strcpy(pdname,name);"
        "struct ParameterDefinition pd = {.name = pdname, .metaType = metaType};"
        "char pdc[sizeof(pd)];"
        "memcpy(pdc, &pd, sizeof(pdc));"
        "C_word* ptr = C_alloc(C_SIZEOF_STRING(sizeof(pdc)));"
        "C_return(C_bytevector(&ptr,sizeof(pdc),pdc));"))

    (define dos_parameterdefinition_name
      (foreign-lambda* c-string ((blob sobj))
        "C_return(((struct ParameterDefinition*)sobj)->name);"))

    ;; SignalDefinition
    (define dos_signaldefinition_create
      (foreign-lambda* scheme-object ((c-string name)
                                      (int parametersCount)
                                      (pointer-vector parameters))
        "struct SignalDefinition sd = {.name = name, .parametersCount = parametersCount, "
        ".parameters = (struct ParameterDefinition*) parameters};"
        "C_word* ptr = C_alloc(1);"
        "C_return(C_structure(&ptr,C_SIZEOF_STRUCTURE(4),sd));"))

    ;; SignalDefinitions
    (define dos_signaldefinitions_create
      (foreign-lambda* (c-pointer SignalDefinitions) ((int count)
                                                      (pointer-vector definitions))
        "struct SignalDefinitions sd = {.count = count, .definitions = (SignalDefinition*) definitions};"
        "struct SignalDefinitions* sdptr = (struct SignalDefinitions*) malloc(sizeof(struct SignalDefinitions));"
        "*sdptr = sd;"
        "C_return(sdptr);"))
    (define dos_signaldefinitions_delete
      (foreign-lambda* void (((c-pointer SignalDefinitions) definitions))
        "free(definitions);"))

    ;; SlotDefinition
    (define dos_slotdefinition_create
      (foreign-lambda* scheme-object ((c-string name)
                                      (int returnMetaType)
                                      (int parametersCount)
                                      (pointer-vector parameters))
        "struct SlotDefinition sd = {.name = name, .returnMetaType = returnMetaType, "
        ".parametersCount = parametersCount, .parameters = (struct ParameterDefinition*) parameters};"
        "C_word* ptr = C_alloc(1);"
        "C_return(C_structure(&ptr,C_SIZEOF_STRUCTURE(5),sd));"))

    ;; SlotDefinitions
    (define dos_slotdefinitions_create
      (foreign-lambda* (c-pointer SlotDefinitions) ((int count)
                                                    (pointer-vector definitions))
        "struct SlotDefinitions sd = {.count = count, .definitions = (SlotDefinition*) definitions};"
        "struct SlotDefinitions* sdptr = (struct SlotDefinitions*) malloc(sizeof(struct SlotDefinitions));"
        "*sdptr = sd;"
        "C_return(sdptr);"))
    (define dos_slotdefinitions_delete
      (foreign-lambda* void (((c-pointer SlotDefinitions) definitions))
        "free(definitions);"))

    ;; PropertyDefinition
    (define dos_propertydefinition_create
      (foreign-lambda* scheme-object ((c-string name)
                                      (int propertyMetaType)
                                      (c-string readSlot)
                                      (c-string writeSlot)
                                      (c-string notifySignal))
        "struct PropertyDefinition pd = {.name = name, .propertyMetaType = propertyMetaType, "
        ".readSlot = readSlot, .writeSlot = writeSlot, .notifySignal = notifySignal};"
        "C_word* ptr = C_alloc(1);"
        "C_return(C_structure(&ptr,C_SIZEOF_STRUCTURE(6),pd));"))

    ;; PropertyDefinitions
    (define dos_propertydefinitions_create
      (foreign-lambda* (c-pointer PropertyDefinitions) ((int count)
                                                        (pointer-vector definitions))
        "struct PropertyDefinitions pd = {.count = count, .definitions = (PropertyDefinition*) definitions};"
        "struct PropertyDefinitions* pdptr = (struct PropertyDefinitions*) malloc(sizeof(struct PropertyDefinitions));"
        "*pdptr = pd;"
        "C_return(pdptr);"))
    (define dos_propertydefinitions_delete
      (foreign-lambda* void (((c-pointer PropertyDefinitions) definitions))
        "free(definitions);"))

    ;; QCoreApplication
    (define dos_qcoreapplication_application_dir_path
      (foreign-lambda c-string "dos_qcoreapplication_application_dir_path"))
    (define dos_qcoreapplication_process_events
      (foreign-safe-lambda* void ((DosQEventLoopProcessEventFlag flags))
        "dos_qcoreapplication_process_events(flags);"
        "qApp->sendPostedEvents();"
        ))
    (define dos_qcoreapplication_process_events_timed
      (foreign-safe-lambda* void ((DosQEventLoopProcessEventFlag flags)
                                  (int ms))
        "dos_qcoreapplication_process_events_timed(flags, ms);"
        "qApp->sendPostedEvents();"
        ))

    ;; QGuiApplication
    (define dos_qguiapplication_create
      (foreign-lambda void "dos_qguiapplication_create"))
    (define dos_qguiapplication_exec
      (foreign-lambda void "dos_qguiapplication_exec"))
    (define dos_qguiapplication_quit
      (foreign-lambda void "dos_qguiapplication_quit"))
    (define dos_qguiapplication_delete
      (foreign-lambda void "dos_qguiapplication_delete"))

    ;; QApplication
    (define dos_qapplication_create
      (foreign-lambda void "dos_qapplication_create"))
    (define dos_qapplication_exec
      (foreign-lambda void "dos_qapplication_exec"))
    (define dos_qapplication_quit
      (foreign-lambda void "dos_qapplication_quit"))
    (define dos_qapplication_delete
      (foreign-lambda void "dos_qapplication_delete"))

    ;; QQmlApplicationEngine
    (define dos_qqmlapplicationengine_create
      (foreign-lambda (c-pointer DosQQmlApplicationEngine) "dos_qqmlapplicationengine_create"))
    (define dos_qqmlapplicationengine_load
      (foreign-lambda void "dos_qqmlapplicationengine_load"
        (c-pointer DosQQmlApplicationEngine)
        c-string))
    (define dos_qqmlapplicationengine_load_url
      (foreign-safe-lambda void "dos_qqmlapplicationengine_load_url"
        (c-pointer DosQQmlApplicationEngine)
        (c-pointer DosQUrl)))
    (define dos_qqmlapplicationengine_load_data
      (foreign-lambda void "dos_qqmlapplicationengine_load_data"
        (c-pointer DosQQmlApplicationEngine)
        c-string))
    (define dos_qqmlapplicationengine_add_import_path
      (foreign-lambda void "dos_qqmlapplicationengine_add_import_path"
        (c-pointer DosQQmlApplicationEngine)
        c-string))
    (define dos_qqmlapplicationengine_context
      (foreign-lambda (c-pointer DosQQmlContext) "dos_qqmlapplicationengine_context"
        (c-pointer DosQQmlApplicationEngine)))
    (define qml_qqmlapplicationengine_root
      (foreign-lambda* (c-pointer DosQObject) (((c-pointer DosQQmlApplicationEngine) engine))
        "C_return((QObject*)((QQmlApplicationEngine*)engine)->rootObjects().first());"))
    (define dos_qqmlapplicationengine_addImageProvider
      (foreign-lambda void "dos_qqmlapplicationengine_addImageProvider"
        (c-pointer DosQQmlApplicationEngine)
        c-string
        (c-pointer DosQQuickImageProvider)))
    (define dos_qqmlapplicationengine_delete
      (foreign-lambda void "dos_qqmlapplicationengine_delete"
        (c-pointer DosQQmlApplicationEngine)))

    ;; QQuickImageProvider
    (define dos_qquickimageprovider_create
      (foreign-lambda* (c-pointer DosQQuickImageProvider) ((RequestPixmapCallback callback))
        "C_return(dos_qquickimageprovider_create((RequestPixmapCallback) callback));"))
    (define dos_qquickimageprovider_delete
      (foreign-lambda* void (((c-pointer DosQQuickImageProvider) vptr))
        "dos_qquickimageprovider_delete(vptr);"))

    ;; QPixmap
    (define dos_qpixmap_create
      (foreign-lambda* (c-pointer DosPixmap) ()
        "C_return(dos_qpixmap_create());"))
    (define dos_qpixmap_create_qpixmap
      (foreign-lambda* (c-pointer DosPixmap) (((c-pointer DosPixmap) other))
        "C_return(dos_qpixmap_create_qpixmap(other));"))
    (define dos_qpixmap_create_width_and_height
      (foreign-lambda* (c-pointer DosPixmap) ((int width)
                                              (int height))
        "C_return(dos_qpixmap_create_width_and_height(width, height));"))
    (define dos_qpixmap_delete
      (foreign-lambda* void (((c-pointer DosPixmap) vptr))
        "dos_qpixmap_delete(vptr);"))
    (define dos_qpixmap_load
      (foreign-lambda* void (((c-pointer DosPixmap) vptr)
                             (c-string filepath)
                             (c-string format))
        "dos_qpixmap_load(vptr, filepath, format);"))
    (define dos_qpixmap_loadFromData
      (foreign-lambda* void (((c-pointer DosPixmap) vptr)
                             (unsigned-c-string data)
                             (unsigned-int len))
        "dos_qpixmap_loadFromData(vptr, data, len);"))
    (define dos_qpixmap_fill
      (foreign-lambda* void (((c-pointer DosPixmap) vptr)
                             (unsigned-char r)
                             (unsigned-char g)
                             (unsigned-char b)
                             (unsigned-char a))
        "dos_qpixmap_fill(vptr, r, g, b, a);"))
    (define dos_qpixmap_assign
      (foreign-lambda* void (((c-pointer DosPixmap) vptr)
                             ((c-pointer DosPixmap) other))
        "dos_qpixmap_assign(vptr, other);"))
    (define dos_qpixmap_isNull
      (foreign-lambda* bool (((c-pointer DosPixmap) vptr))
        "C_return(dos_qpixmap_isNull(vptr));"))

    ;; QQuickStyle
    (define dos_qquickstyle_set_style
      (foreign-lambda* void ((c-string style))
        "dos_qquickstyle_set_style(style);"))
    (define dos_qquickstyle_set_fallback_style
      (foreign-lambda* void ((c-string style))
        "dos_qquickstyle_set_fallback_style(style);"))

    ;; QQuickView
    (define dos_qquickview_create
      (foreign-lambda* (c-pointer DosQQuickView) ()
        "C_return(dos_qquickview_create());"))
    (define dos_qquickview_show
      (foreign-lambda* void (((c-pointer DosQQuickView) vptr))
        "dos_qquickview_show(vptr);"))
    (define dos_qquickview_source
      (foreign-lambda* c-string (((c-pointer DosQQuickView) vptr))
        "dos_qquickview_source(vptr);"))
    (define dos_qquickview_set_source_url
      (foreign-lambda* void (((c-pointer DosQQuickView) vptr)
                             ((c-pointer DosQUrl) url))
        "dos_qquickview_set_source_url(vptr, url);"))
    (define dos_qquickview_set_source
      (foreign-lambda* void (((c-pointer DosQQuickView) vptr)
                             (c-string filename))
        "dos_qquickview_set_source(vptr, filename);"))
    (define dos_qquickview_set_resize_mode
      (foreign-lambda* void (((c-pointer DosQQuickView) vptr)
                             (int resizeMode))
        "dos_qquickview_set_resize_mode(vptr, resizeMode);"))
    (define dos_qquickview_delete
      (foreign-lambda* void (((c-pointer DosQQuickView) vptr))
        "dos_qquickview_delete(vptr);"))
    (define dos_qquickview_rootContext
      (foreign-lambda* (c-pointer DosQQmlContext) (((c-pointer DosQQuickView) vptr))
        "C_return(dos_qquickview_rootContext(vptr));"))

    ;; QQmlContext
    (define dos_qqmlcontext_baseUrl
      (foreign-lambda* c-string (((c-pointer DosQQmlContext) vptr))
        "C_return(dos_qqmlcontext_baseUrl(vptr));"))
    (define dos_qqmlcontext_setcontextproperty
      (foreign-lambda* void (((c-pointer DosQQmlContext) vptr)
                             (c-string name)
                             ((c-pointer DosQVariant) value))
        "dos_qqmlcontext_setcontextproperty(vptr, name, value);"))

    ;; String
    (define dos_chararray_delete
      (foreign-lambda* void ((c-string ptr))
        "dos_chararray_delete(ptr);"))

    ;; QVariant
    (define dos_qvariantarray_create
      (foreign-lambda* (c-pointer DosQVariantArray) ((int size)
                                                     (pointer-vector data))
        "struct DosQVariantArray qva = "
        "{.size = size, .data = data};"
        "struct DosQVariantArray* qvaptr = (struct DosQVariantArray*) malloc(sizeof(struct DosQVariantArray));"
        "*qvaptr = qva;"
        "C_return(qvaptr);"))
    (define dos_qvariantarray_delete
      (foreign-lambda* void (((c-pointer DosQVariantArray) ptr))
        "dos_qvariantarray_delete(ptr);"))
    (define dos_qvariant_create
      (foreign-lambda* (c-pointer DosQVariant) ()
        "C_return(dos_qvariant_create());"))
    (define dos_qvariant_create_int
      (foreign-lambda* (c-pointer DosQVariant) ((int value))
        "C_return(dos_qvariant_create_int(value));"))
    (define dos_qvariant_create_longlong
      (foreign-lambda* (c-pointer DosQVariant) ((long value))
        "C_return(dos_qvariant_create_longlong(value));"))
    (define dos_qvariant_create_ulonglong
      (foreign-lambda* (c-pointer DosQVariant) ((unsigned-long value))
        "C_return(dos_qvariant_create_ulonglong(value));"))
    (define dos_qvariant_create_bool
      (foreign-lambda* (c-pointer DosQVariant) ((bool value))
        "C_return(dos_qvariant_create_bool(value));"))
    (define dos_qvariant_create_string
      (foreign-lambda* (c-pointer DosQVariant) ((c-string value))
        "C_return(dos_qvariant_create_string(value));"))
    (define dos_qvariant_create_qobject
      (foreign-lambda* (c-pointer DosQVariant) (((c-pointer DosQObject) value))
        "C_return(dos_qvariant_create_qobject(value));"))
    (define dos_qvariant_create_qvariant
      (foreign-lambda* (c-pointer DosQVariant) (((c-pointer DosQVariant) value))
        "C_return(dos_qvariant_create_qvariant(value));"))
    (define dos_qvariant_create_float
      (foreign-lambda* (c-pointer DosQVariant) ((float value))
        "C_return(dos_qvariant_create_float(value));"))
    (define dos_qvariant_create_double
      (foreign-lambda* (c-pointer DosQVariant) ((double value))
        "C_return(dos_qvariant_create_double(value));"))
    (define dos_qvariant_create_array
      (foreign-lambda* (c-pointer DosQVariant) ((int size)
                                                (pointer-vector array))
        "C_return(dos_qvariant_create_array(size, (DosQVariant**) array));"))

    (define dos_qvariant_setInt
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             (int value))
        "dos_qvariant_setInt(vptr, value);"))
    (define dos_qvariant_setLongLong
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             (long value))
        "dos_qvariant_setLongLong(vptr, value);"))
    (define dos_qvariant_setULongLong
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             (unsigned-long value))
        "dos_qvariant_setULongLong(vptr, value);"))
    (define dos_qvariant_setBool
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             (bool value))
        "dos_qvariant_setBool(vptr, value);"))
    (define dos_qvariant_setFloat
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             (float value))
        "dos_qvariant_setFloat(vptr, value);"))
    (define dos_qvariant_setDouble
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             (double value))
        "dos_qvariant_setDouble(vptr, value);"))
    (define dos_qvariant_setString
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             (c-string value))
        "dos_qvariant_setString(vptr, value);"))
    (define dos_qvariant_setQObject
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             ((c-pointer DosQObject) value))
        "dos_qvariant_setQObject(vptr, value);"))
    (define dos_qvariant_setArray
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             (int size)
                             (pointer-vector array))
        "dos_qvariant_setArray(vptr, size, array);"))
    (define dos_qvariant_isnull
      (foreign-lambda* bool (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_isnull(vptr));"))
    (define dos_qvariant_delete
      (foreign-lambda* void (((c-pointer DosQVariant) vptr))
        "dos_qvariant_delete(vptr);"))
    (define dos_qvariant_assign
      (foreign-lambda* void (((c-pointer DosQVariant) vptr)
                             ((c-pointer DosQVariant) other))
        "dos_qvariant_assign(vptr, other);"))
    (define dos_qvariant_toInt
      (foreign-lambda* int (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toInt(vptr));"))
    (define dos_qvariant_toLongLong
      (foreign-lambda* long (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toLongLong(vptr));"))
    (define dos_qvariant_toULongLong
      (foreign-lambda* unsigned-long (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toULongLong(vptr));"))
    (define dos_qvariant_toBool
      (foreign-lambda* bool (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toBool(vptr));"))
    (define dos_qvariant_toString
      (foreign-lambda* c-string (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toString(vptr));"))
    (define dos_qvariant_toFloat
      (foreign-lambda* float (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toFloat(vptr));"))
    (define dos_qvariant_toDouble
      (foreign-lambda* double (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toDouble(vptr));"))
    (define dos_qvariant_toArray
      (foreign-lambda* (c-pointer DosQVariantArray) (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toArray(vptr));"))
    (define dos_qvariant_toQObject
      (foreign-lambda* (c-pointer DosQObject) (((c-pointer DosQVariant) vptr))
        "C_return(dos_qvariant_toQObject(vptr));"))

    ;; QMetaObject
    (define dos_qmetaobject_create
      (foreign-lambda* (c-pointer DosQMetaObject) (((c-pointer DosQMetaObject) superClassMetaObject)
                                                   (c-string className)
                                                   ((c-pointer SignalDefinitions) signalDefinitions)
                                                   ((c-pointer SlotDefinitions) slotDefinitions)
                                                   ((c-pointer PropertyDefinitions) propertyDefinitions))
        "C_return(dos_qmetaobject_create(superClassMetaObject, className, signalDefinitions, slotDefinitions, propertyDefinitions));"))
    (define dos_qmetaobject_delete
      (foreign-lambda* void (((c-pointer DosQMetaObject) vptr))
        "dos_qmetaobject_delete(vptr);"))
    (define dos_qmetaobject_invoke_method
      (foreign-lambda* bool (((c-pointer DosQObject) context)
                             (DosQMetaObjectInvokeMethodCallback callback)
                             (c-pointer data)
                             (DosQtConnectionType connection_type))
        "C_return(dos_qmetaobject_invoke_method(context, callback, data, connection_type));"))

    ;; QMetaObjectConnection
    (define dos_qmetaobject_connection_delete
      (foreign-lambda void "dos_qmetaobject_connection_delete" (c-pointer DosQMetaObjectConnection)))

    ;; QAbstractListModel
    (define dos_qabstractlistmodel_qmetaobject
      (foreign-lambda* (c-pointer DosQMetaObject) ()
        "C_return(dos_qabstractlistmodel_qmetaobject());"))
    (define dos_qabstractlistmodel_create
      (foreign-lambda*
          (c-pointer DosQAbstractListModel) (((c-pointer void) callbackObject)
                                             ((c-pointer DosQMetaObject) metaObject)
                                             (DObjectCallback dObjectCallback)
                                             ((c-pointer DosQAbstractItemModelCallbacks) callbacks))
        "C_return(dos_qabstractlistmodel_create(callbackObject, metaObject, dObjectCallback, callbacks));"))
    (define dos_qabstractlistmodel_index
      (foreign-lambda* (c-pointer DosQModelIndex) (((c-pointer DosQAbstractListModel) vptr)
                                                   (int row)
                                                   (int column)
                                                   ((c-pointer DosQModelIndex) parent))
        "C_return(dos_qabstractlistmodel_index(vptr, row, column, parent));"))
    (define dos_qabstractlistmodel_parent
      (foreign-lambda* (c-pointer DosQModelIndex) (((c-pointer DosQAbstractListModel) vptr)
                                                   ((c-pointer DosQModelIndex) child))
        "C_return(dos_qabstractlistmodel_parent(vptr, child));"))
    (define dos_qabstractlistmodel_columnCount
      (foreign-lambda* int (((c-pointer DosQAbstractListModel) vptr)
                            ((c-pointer DosQModelIndex) parent))
        "C_return(dos_qabstractlistmodel_columnCount(vptr, parent));"))

    ;; QAbstractTableModel
    (define dos_qabstracttablemodel_qmetaobject
      (foreign-lambda* (c-pointer DosQMetaObject) ()
        "C_return(dos_qabstracttablemodel_qmetaobject());"))
    (define dos_qabstracttablemodel_create
      (foreign-safe-lambda*
          (c-pointer DosQAbstractTableModel) (((c-pointer void) callbackObject)
                                              ((c-pointer DosQMetaObject) metaObject)
                                              (DObjectCallback dObjectCallback)
                                              ((c-pointer DosQAbstractItemModelCallbacks) callbacks))
        "C_return(dos_qabstracttablemodel_create(callbackObject, metaObject, dObjectCallback, callbacks));"))
    (define dos_qabstracttablemodel_index
      (foreign-lambda* (c-pointer DosQModelIndex) (((c-pointer DosQAbstractTableModel) vptr)
                                                   (int row)
                                                   (int column)
                                                   ((c-pointer DosQModelIndex) parent))
        "C_return(dos_qabstracttablemodel_index(vptr, row, column, parent));"))
    (define dos_qabstracttablemodel_parent
      (foreign-lambda* (c-pointer DosQModelIndex) (((c-pointer DosQAbstractTableModel) vptr)
                                                   ((c-pointer DosQModelIndex) child))
        "C_return(dos_qabstracttablemodel_parent(vptr, child));"))

    ;; QAbstractItemModel
    (define dos_qabstractitemmodel_qmetaobject
      (foreign-lambda* (c-pointer DosQMetaObject) ()
        "C_return(dos_qabstractitemmodel_qmetaobject());"))
    (define dos_qabstractitemmodel_create
      (foreign-safe-lambda*
          (c-pointer DosQAbstractItemModel) (((c-pointer void) callbackObject)
                                             ((c-pointer DosQMetaObject) metaObject)
                                             (DObjectCallback dObjectCallback)
                                             ((c-pointer DosQAbstractItemModelCallbacks) callbacks))
        "C_return(dos_qabstractitemmodel_create(callbackObject, metaObject, dObjectCallback, callbacks));"))
    (define dos_qabstractitemmodel_setData
      (foreign-lambda* bool (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) index)
                             ((c-pointer DosQVariant) data)
                             (int role))
        "C_return(dos_qabstractitemmodel_setData(vptr, index, data, role));"))
    (define dos_qabstractitemmodel_roleNames
      (foreign-lambda* (c-pointer DosQHashIntQByteArray) (((c-pointer DosQAbstractItemModel) vptr))
        "C_return(dos_qabstractitemmodel_roleNames(vptr));"))
    (define dos_qabstractitemmodel_flags
      (foreign-lambda* int (((c-pointer DosQAbstractItemModel) vptr)
                            ((c-pointer DosQModelIndex) index))
        "C_return(dos_qabstractitemmodel_flags(vptr, index));"))
    (define dos_qabstractitemmodel_headerData
      (foreign-lambda* (c-pointer DosQVariant) (((c-pointer DosQAbstractItemModel) vptr)
                                                (int section)
                                                (int orientation)
                                                (int role))
        "C_return(dos_qabstractitemmodel_headerData(vptr, section, orientation, role));"))
    (define dos_qabstractitemmodel_hasChildren
      (foreign-lambda* bool (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) parentIndex))
        "C_return(dos_qabstractitemmodel_hasChildren(vptr, parentIndex));"))
    (define dos_qabstractitemmodel_hasIndex
      (foreign-lambda* bool (((c-pointer DosQAbstractItemModel) vptr)
                             (int row)
                             (int column)
                             ((c-pointer DosQModelIndex) dosParentIndex))
        "C_return(dos_qabstractitemmodel_hasIndex(vptr, row, column, dosParentIndex));"))
    (define dos_qabstractitemmodel_canFetchMore
      (foreign-lambda* bool (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) parentIndex))
        "C_return(dos_qabstractitemmodel_canFetchMore(vptr, parentIndex));"))
    (define dos_qabstractitemmodel_fetchMore
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) parentIndex))
        "dos_qabstractitemmodel_fetchMore(vptr, parentIndex);"))
    (define dos_qabstractitemmodel_beginInsertRows
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) parent)
                             (int first)
                             (int last))
        "dos_qabstractitemmodel_beginInsertRows(vptr, parent, first, last);"))
    (define dos_qabstractitemmodel_endInsertRows
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr))
        "dos_qabstractitemmodel_endInsertRows(vptr);"))
    (define dos_qabstractitemmodel_beginRemoveRows
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) parent)
                             (int first)
                             (int last))
        "dos_qabstractitemmodel_beginRemoveRows(vptr, parent, first, last);"))
    (define dos_qabstractitemmodel_endRemoveRows
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr))
        "dos_qabstractitemmodel_endRemoveRows(vptr);"))
    (define dos_qabstractitemmodel_beginInsertColumns
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) parent)
                             (int first)
                             (int last))
        "dos_qabstractitemmodel_beginInsertColumns(vptr, parent, first, last);"))
    (define dos_qabstractitemmodel_endInsertColumns
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr))
        "dos_qabstractitemmodel_endInsertColumns(vptr);"))
    (define dos_qabstractitemmodel_beginRemoveColumns
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) parent)
                             (int first)
                             (int last))
        "dos_qabstractitemmodel_beginRemoveColumns(vptr, parent, first, last);"))
    (define dos_qabstractitemmodel_endRemoveColumns
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr))
        "dos_qabstractitemmodel_endRemoveColumns(vptr);"))
    (define dos_qabstractitemmodel_beginResetModel
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr))
        "dos_qabstractitemmodel_beginResetModel(vptr);"))
    (define dos_qabstractitemmodel_endResetModel
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr))
        "dos_qabstractitemmodel_endResetModel(vptr);"))
    (define dos_qabstractitemmodel_dataChanged
      (foreign-lambda* void (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) topLeft)
                             ((c-pointer DosQModelIndex) bottomRight)
                             ((c-pointer int) rolesPtr)
                             (int rolesLength))
        "dos_qabstractitemmodel_dataChanged(vptr, topLeft, bottomRight, rolesPtr, rolesLength);"))
    (define dos_qabstractitemmodel_createIndex
      (foreign-lambda* (c-pointer DosQModelIndex) (((c-pointer DosQAbstractItemModel) vptr)
                                                   (int row)
                                                   (int column)
                                                   (c-pointer data))
        "C_return(dos_qabstractitemmodel_createIndex(vptr, row, column, data));"))
    (define dos_qabstractitemmodel_setData
      (foreign-lambda* bool (((c-pointer DosQAbstractItemModel) vptr)
                             ((c-pointer DosQModelIndex) index)
                             ((c-pointer DosQVariant) value)
                             (int role))
        "C_return(dos_qabstractitemmodel_setData(vptr, index, value, role));"))
    (define dos_qabstractitemmodel_roleNames
      (foreign-lambda* (c-pointer DosQHashIntQByteArray) (((c-pointer DosQAbstractItemModel) vptr))
        "C_return(dos_qabstractitemmodel_roleNames(vptr));"))
    (define dos_qabstractitemmodel_flags
      (foreign-lambda* int (((c-pointer DosQAbstractItemModel) vptr)
                            ((c-pointer DosQModelIndex) index))
        "C_return(dos_qabstractitemmodel_flags(vptr, index));"))
    (define dos_qabstractitemmodel_headerData
      (foreign-lambda* (c-pointer DosQVariant) (((c-pointer DosQAbstractItemModel) vptr)
                                                (int section)
                                                (int orientation)
                                                (int role))
        "C_return(dos_qabstractitemmodel_headerData(vptr, section, orientation, role));"))

    ;; QObject
    (define dos_qobject_qmetaobject
      (foreign-lambda* (c-pointer DosQMetaObject) ()
        "C_return(dos_qobject_qmetaobject());"))
    (define dos_qobject_create
      (foreign-lambda (c-pointer DosQObject) "dos_qobject_create"
        c-pointer (c-pointer DosQMetaObject) DObjectCallback))
    (define dos_qobject_signal_emit
      (foreign-lambda void "dos_qobject_signal_emit"
        (c-pointer DosQObject) c-string int (c-pointer c-pointer)))
    (define dos_qobject_objectName
      (foreign-lambda c-string "dos_qobject_objectName"
        (c-pointer DosQObject)))
    (define dos_qobject_setObjectName
      (foreign-lambda void "dos_qobject_setObjectName"
        (c-pointer DosQObject) c-string))
    (define dos_qobject_delete
      (foreign-lambda void "dos_qobject_delete"
        (c-pointer DosQObject)))
    (define dos_qobject_deleteLater
      (foreign-lambda void "dos_qobject_deleteLater"
        (c-pointer DosQObject)))
    (define dos_qobject_property
      (foreign-lambda (c-pointer DosQVariant) "dos_qobject_property"
        (c-pointer DosQObject) c-string))
    (define dos_qobject_setProperty
      (foreign-lambda bool "dos_qobject_setProperty"
        (c-pointer DosQObject) c-string (c-pointer DosQVariant)))
    (define qml_qobject_findChild
      (foreign-lambda* (c-pointer DosQObject) (((c-pointer DosQObject) qobj)
                                               (c-string name))
        "C_return((DosQObject*)((QObject*)qobj)->findChild<QObject*>(name));"))

    (define dos_qobject_connect_lambda_static
      (foreign-lambda* (c-pointer DosQMetaObjectConnection) (((c-pointer DosQObject) sender)
                                                             (c-string signal)
                                                             ((function void (c-pointer int (c-pointer (c-pointer DosQVariant)))) callback)
                                                             (c-pointer callbackData)
                                                             (DosQtConnectionType connection_type))
        "C_return(dos_qobject_connect_lambda_static(sender, dos_signal_macro(signal), callback, callbackData, connection_type));"))
    (define dos_qobject_connect_lambda_with_context_static
      (foreign-lambda* (c-pointer DosQMetaObjectConnection) (((c-pointer DosQObject) sender)
                                                             (c-string signal)
                                                             ((c-pointer DosQObject) context)
                                                             ((function void (c-pointer int (c-pointer (c-pointer DosQVariant)))) callback)
                                                             (c-pointer callbackData)
                                                             (DosQtConnectionType connection_type))
        "C_return(dos_qobject_connect_lambda_with_context_static(sender, dos_signal_macro(signal), context, callback, callbackData, connection_type));"))
    (define dos_qobject_connect_static
      (foreign-lambda* (c-pointer DosQMetaObjectConnection) (((c-pointer DosQObject) sender)
                                                             (c-string signal)
                                                             ((c-pointer DosQObject) receiver)
                                                             (c-string slot)
                                                             (DosQtConnectionType connection_type))
        "C_return(dos_qobject_connect_static(sender, dos_signal_macro(signal), receiver, dos_slot_macro(slot), connection_type));"))
    (define dos_qobject_disconnect_static
      (foreign-lambda* void (((c-pointer DosQObject) sender)
                             (c-string signal)
                             ((c-pointer DosQObject) receiver)
                             (c-string slot))
        "dos_qobject_disconnect_static(sender, dos_signal_macro(signal), receiver, dos_slot_macro(slot));"))
    (define dos_qobject_disconnect_with_connection_static
      (foreign-lambda void "dos_qobject_disconnect_with_connection_static"
        (c-pointer DosQMetaObjectConnection)))

    ;; QModelIndex
    (define dos_qmodelindex_create
      (foreign-lambda (c-pointer DosQModelIndex) "dos_qmodelindex_create"))
    (define dos_qmodelindex_create_qmodelindex
      (foreign-lambda (c-pointer DosQModelIndex) "dos_qmodelindex_create_qmodelindex"
        (c-pointer DosQModelIndex)))
    (define dos_qmodelindex_delete
      (foreign-lambda void "dos_qmodelindex_delete"
        (c-pointer DosQModelIndex)))
    (define dos_qmodelindex_row
      (foreign-lambda int "dos_qmodelindex_row"
        (c-pointer DosQModelIndex)))
    (define dos_qmodelindex_column
      (foreign-lambda int "dos_qmodelindex_column"
        (c-pointer DosQModelIndex)))
    (define dos_qmodelindex_isValid
      (foreign-lambda bool "dos_qmodelindex_isValid"
        (c-pointer DosQModelIndex)))
    (define dos_qmodelindex_data
      (foreign-lambda (c-pointer DosQVariant) "dos_qmodelindex_data"
        (c-pointer DosQModelIndex)
        int))
    (define dos_qmodelindex_parent
      (foreign-lambda (c-pointer DosQModelIndex) "dos_qmodelindex_parent"
        (c-pointer DosQModelIndex)))
    (define dos_qmodelindex_child
      (foreign-lambda (c-pointer DosQModelIndex) "dos_qmodelindex_child"
        (c-pointer DosQModelIndex)
        int
        int))
    (define dos_qmodelindex_sibling
      (foreign-lambda (c-pointer DosQModelIndex) "dos_qmodelindex_sibling"
        (c-pointer DosQModelIndex)
        int
        int))
    (define dos_qmodelindex_assign
      (foreign-lambda void "dos_qmodelindex_assign"
        (c-pointer DosQModelIndex)
        (c-pointer DosQModelIndex)))
    (define dos_qmodelindex_internalPointer
      (foreign-lambda c-pointer "dos_qmodelindex_internalPointer"
        (c-pointer DosQModelIndex)))

    ;; QHash
    (define dos_qhash_int_qbytearray_create
      (foreign-lambda (c-pointer DosQHashIntQByteArray) "dos_qhash_int_qbytearray_create"))
    (define dos_qhash_int_qbytearray_delete
      (foreign-lambda void "dos_qhash_int_qbytearray_delete"
        (c-pointer DosQHashIntQByteArray)))
    (define dos_qhash_int_qbytearray_insert
      (foreign-lambda void "dos_qhash_int_qbytearray_insert"
        (c-pointer DosQHashIntQByteArray)
        int
        c-string))
    (define dos_qhash_int_qbytearray_value
      (foreign-lambda c-string "dos_qhash_int_qbytearray_value"
        (c-pointer DosQHashIntQByteArray)
        int))

    ;; QResource
    (define dos_qresource_register
      (foreign-lambda void "dos_qresource_register"
        c-string))

    ;; QUrl
    (define dos_qurl_create
      (foreign-lambda (c-pointer DosQUrl) "dos_qurl_create"
        c-string
        int))
    (define dos_qurl_delete
      (foreign-lambda void "dos_qurl_delete"
        (c-pointer DosQUrl)))
    (define dos_qurl_to_string
      (foreign-lambda c-string "dos_qurl_to_string"
        (c-pointer DosQUrl)))
    (define dos_qurl_isValid
      (foreign-lambda bool "dos_qurl_isValid"
        (c-pointer DosQUrl)))

    ;; QmlRegisterType
    (define dos_qmlregistertype_create
      (foreign-lambda* (c-pointer QmlRegisterType) ((int major)
                                                    (int minor)
                                                    (c-string uri)
                                                    (c-string qml)
                                                    ((c-pointer DosQMetaObject) staticMetaObject)
                                                    (CreateDObject createDObject)
                                                    (DeleteDObject deleteDObject))
        "struct QmlRegisterType qrt = {.major = major, .minor = minor, .uri = uri, .qml = qml,"
        ".staticMetaObject = staticMetaObject, .createDObject = createDObject, .deleteDObject = deleteDObject};"
        "struct QmlRegisterType* qrtptr = (struct QmlRegisterType*) malloc(sizeof(struct QmlRegisterType));"
        "*qrtptr = qrt;"
        "C_return(qrtptr);"))

    ;; QDeclarative
    (define dos_qdeclarative_qmlregistertype
      (foreign-lambda int "dos_qdeclarative_qmlregistertype"
        (c-pointer QmlRegisterType)))
    (define dos_qdeclarative_qmlregistersingletontype
      (foreign-lambda int "dos_qdeclarative_qmlregistersingletontype"
        (c-pointer QmlRegisterType)))

    ;; QPointer
    (define dos_qpointer_create
      (foreign-lambda (c-pointer DosQPointer) "dos_qpointer_create"
        (c-pointer DosQObject)))
    (define dos_qpointer_delete
      (foreign-lambda void "dos_qpointer_delete"
        (c-pointer DosQPointer)))
    (define dos_qpointer_is_null
      (foreign-lambda bool "dos_qpointer_is_null"
        (c-pointer DosQPointer)))
    (define dos_qpointer_clear
      (foreign-lambda void "dos_qpointer_clear"
        (c-pointer DosQPointer)))
    (define dos_qpointer_data
      (foreign-lambda (c-pointer DosQObject) "dos_qpointer_data"
        (c-pointer DosQPointer)))))
