import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

Window {
  id: window
  objectName: "window"
  visible: true
  width: 800
  height: 600
  signal close
  onVisibilityChanged: {
    if(visibility == Window.Hidden)
      close()
  }
  ColumnLayout {
    id: rowlayout
    anchors.fill: parent
    spacing: 30
    Rectangle {
      id: toprectangle
      Layout.fillWidth: true
      Layout.preferredHeight: 240
      color: "lightgray"
      Text {
        id: helloText
        objectName: "helloText"
        text: "Hello World!"
        y: 30
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 24; font.bold: true
      }
    }
    RowLayout {
      spacing: 15
      Layout.fillWidth: true
      Layout.fillHeight: true
      Text {
        text: qsTr("Change the text above:")
      }
      TextField {
        id: nameInputField
        objectName: "nameInputField"
        placeholderText: qsTr("Enter your name")
      }
    }
  }
}
