(import
  (chicken base)
  srfi-18
  (qml core)
  coops)

(gui-application-create)
(define engine (make <QQmlApplicationEngine>))

(define (windowLoadCallback cbdata argv) void
  (print cbdata)
  (print "Loaded QML file: " (to (cadr argv) string:)))

(define callback-data "Test")
(define textchange-cbdata "Text Edited")

(define conn (connect engine "objectCreated(QObject*,QUrl)"
                      windowLoadCallback
                      callback-data
                      (qt-connection-type auto:)))

(define loc (new-QUrl "examples/helloworld.qml"))
(load-url engine loc)

(define nameInputField (find-child (root engine) "nameInputField"))
(define helloText (find-child (root engine) "helloText"))
;; TODO: Why does the following not work?
(define window (find-child (root engine) "window"))

(define (textChangeCallback cbdata argv) void
  (print "Text changed to " (to (property nameInputField "text") string:))
  (set-property helloText "text" (property nameInputField "text")))

(define (closeProgram cbdata argv) void
  (print "Window closed, exiting program.")
  (exit 0))

(define conn2 (connect nameInputField "textEdited()"
                       textChangeCallback
                       textchange-cbdata
                       (qt-connection-type auto:)))

(define conn3 (connect (root engine) "close()"
                       closeProgram #f
                       (qt-connection-type auto:)))

(do ((loop #t))
  ((not loop) #t)
  (thread-sleep! (seconds->time (+ 0.05 (time->seconds (current-time)))))
  (process-events-timed (qevent-loop-process-event-flag process-all-events:) 50))
