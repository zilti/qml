#!/bin/sh

rm -rf report
salmonella --keep-repo --repo-dir=./petri-dish
salmonella-html-report ./salmonella.log report
